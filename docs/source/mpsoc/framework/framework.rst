.. _software_framework:

==================
Software Framework
==================

.. toctree::
    :maxdepth: 2
    :caption: Software Framework

    software_development_guidlines/software_development_guidlines
    hardwareAbstractionLayer/hardwareAbstractionLayer
    array/array
    dq_transformation/dq_transformation
    FOC/FOC
    linear_decoupling/linear_decoupling
    piController/piController
    PMSM_config/uz_PMSM_config
    signals/signals
    space_vector_limitation/space_vector_limitation
    SpeedControl/SpeedControl
    wavegen/wavegen
    global_configuration/global_configuration
    unit_tests/unit_tests
    assertions/assertions
    SystemTime/SystemTime
    matrix/matrix_math
    neural_network/neural_network
