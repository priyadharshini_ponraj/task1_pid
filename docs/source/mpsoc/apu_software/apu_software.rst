.. _apu_software:

============
APU Software
============

APU runs FreeRTOS to read and write data to/from RPU and transfer it to a host-pc by using ethernet.

..	toctree::
    :maxdepth: 2
    :caption: APU Software

    datamover_a53

