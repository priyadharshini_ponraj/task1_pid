.. _ip_cores:

========
IP Cores
========


..	toctree::
    :maxdepth: 2
    :caption: IP Cores
    
    pwm_and_ss_control
    uz_PWM_SS_2L/uz_pwm_ss_2l
    adc_LTC2311
    adc_LTC2311_v3
    uz_incrementalEncoder/uz_incrementalEncoder
    interlock
    axi_testIP/axi_testIP
    uz_interlockDeadtime2L/uz_interlockDeadtime2L
    uz_plantModel_pt1/uz_plantModel_pt1
    uz_pmsmModel/uz_pmsmModel
    uz_dataMover/uz_dataMover
