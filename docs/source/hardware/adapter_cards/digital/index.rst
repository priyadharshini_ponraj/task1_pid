=========================
Digital Adapter Boards
=========================
..	toctree::
	:maxdepth: 2
	:caption: Digital

	optical_general
	incr_encoder_v1
	digital_voltage_3u
	Digital_BreakoutBoard_v1
	encoder_v1