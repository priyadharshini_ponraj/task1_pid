.. _Git:

===
Git
===

Installation
------------

The version control of the source files of the UltraZohm is done by git.
Therefore, a git installation as well as git LFS is required:

#. https://git-scm.com/downloads
#. https://git-lfs.github.com/

The homepage of git offers excellent documentation (https://git-scm.com/doc), tutorials, and videos (https://git-scm.com/videos).
Another recommended tutorial is offered by Bitbucket (https://www.atlassian.com/git/tutorials).
An understanding of git is mandatory for working with the UltraZohm and is assumed in the following sections!

A graphical git client can be used to simplify the usage of git.
`Sourcetree <https://www.sourcetreeapp.com/>`_ (Free, Windows and Mac) or `GitKraken <https://www.gitkraken.com>`_ (paid for private repositories, free for students and research institutions/non-profit, Windows, Mac, Linux) is recommended.

Git Bash
--------

If you are comfortable with git bash / CLI tool you can use it without the GUI client.

.. image:: ./images_git/gitclone.png
