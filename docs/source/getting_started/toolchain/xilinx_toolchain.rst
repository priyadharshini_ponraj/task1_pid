.. _XilinxToolchain:

================
Xilinx Toolchain
================

1. `Download Link <https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/vitis.html>`_ (Xilinx account required)

.. image:: ./images_installation/vitis_website1_2020.png

2. Choose Name, Address, E-Mail, and the rest of the data
3. File ``Xilinx_Unified_2020.1_0602_1208_Win64.exe`` run. Click Next
4. Select Download and Install Now and sign in with your Xilinx user account. Click Next

.. image:: ./images_installation/vitis_website2_2020.png

If the User Authentication in the installer results in an error, eventhough the Xilinx account is valid, an alternative installer without the need to sign in can be downloaded.
   
.. image:: ./images_installation/vitis_website2_5_2020.png

5. Agree to all license agreements, terms, and conditions. Click Next

.. image:: ./images_installation/vitis_website3_2020.png

6. Select the specific products

.. image:: ./images_installation/vitis_website4_2020.png

7. Install what you want and what you need. It is recommended to install as shown below. Uncheck `System Generator for DSP` and do not install it because this is not used and can lead to errors in MATLAB since this is a MATLAB-Toolbox.

.. image:: ./images_installation/vitis_website5_2020.png

8. Specify where to install

.. image:: ./images_installation/vitis_website6_2020.png

9. Install

.. image:: ./images_installation/vitis_website7_2020.png

10. Install the ``Vitis Core Development Kit Update 1`` in the same way
