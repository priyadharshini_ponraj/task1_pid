======================
Toolchain installation
======================

The basic toolchain consists of:

1. Xilinx Vitis & Vivado Toolchain
2. git
3. Sourcetree
4. Lattice CPLD Toolchain

..	toctree::
	:maxdepth: 2
	:caption: Toolchain

	xilinx_toolchain
	license
	git
	sourcetree
	lattice
