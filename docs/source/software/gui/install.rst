=======
Install
=======

Install Java JDK to use GUI
----------------------------

1. `Download <https://jdk.java.net/l>`_ and install newest JDK version for your OS from Open jdk
2. Unzip the folder and move it to your program files (e.g. ``C:\Program Files\Java``)
3. Add your JDK installation folder (e.g. ``C:\Program Files\Java\jdk-16\bin``) to your system PATH variable;
4. Open your terminal and check the installed java version:

..	image:: ./images_install/terminal_check_java_version.png