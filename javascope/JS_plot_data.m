%% clean Matlab Workspace
close all
clear all
format compact

%% import latest csv
Logfile_list = dir('Log_*');
%chose latest logfile which is not empty
Logfile_list_size = Logfile_list.bytes;
for logfile_list_index = size(Logfile_list,1):-1:1
    if (Logfile_list(logfile_list_index).bytes>130)
        break;
    else 
        % delete the most recent empty logfiles
        delete [Logfile_list(logfile_list_index).folder,'\',Logfile_list(logfile_list_index).name]
        disp(['Deleted file: ', Logfile_list(logfile_list_index).name])
    end
end
file_name = Logfile_list(logfile_list_index).name

% paste file name here if you want to open a specific file
%file_name = Log_2021-09-15_16-59-35.csv;

% read csv 
log = readtable(file_name);

%% Clean up of CSV
% get names of entries in log table
channel_names  = log.Properties.VariableNames;

% extract selected channel names from CSV
table_variable_names = readtable(file_name,'Range','A5:ZZ6');
variable_names = table_variable_names.Properties.VariableNames;

% read number of channels excluding time stamp generated by javascope
num_channels = size(log,2)-1;

%delete NaNs - in case of incomplete rows
toDelete = isnan(log.CH20);
log(toDelete,:) = [];

% let time start from 0
n_samples_full = size(log.time,1);
log.time(:) = log.time - log.time(1);

%% Plot all channels
for ch = 1:num_channels
    figure(ch)
%     hold on
    plot_command = ['plot(log.',channel_names{ch+1},');']; 
    eval(plot_command)
    title(variable_names{ch+1}, 'Interpreter', 'none') %first entry is the time stamp
end

%% Useful subplots with synchronized x-axis zoom
nplots = 4; %choose how many plots in one figure
for plots = 1:ceil(num_channels/nplots)
    figure(100+plots)
    tiledlayout(nplots,1)
    
    for plot_ch=1:nplots
        ax(plot_ch) = nexttile;
        hold on
        ch = plot_ch + nplots*(plots-1);
        if(ch>num_channels)
            break
        end
        plot_command = ['plot(log.',channel_names{ch+1},');'];        
        eval(plot_command)
        title(variable_names{ch+1}, 'Interpreter', 'none') %first entry is the time stamp
    end
    %synochronize x-axis zoom
    linkaxes(ax,'x')
end

%% rename channels - if needed - done manually by user!
log = renamevars(log, "CH1",  "CH1" );
log = renamevars(log, "CH2",  "CH2" );
log = renamevars(log, "CH3",  "CH3" );
log = renamevars(log, "CH4",  "CH4" );
log = renamevars(log, "CH5",  "CH5" );
log = renamevars(log, "CH6",  "CH6" );
log = renamevars(log, "CH7",  "CH7" );
log = renamevars(log, "CH8",  "CH8" );
log = renamevars(log, "CH9",  "CH9" );
log = renamevars(log, "CH10", "CH10");
log = renamevars(log, "CH11", "CH11");
log = renamevars(log, "CH12", "CH12");
log = renamevars(log, "CH13", "CH13");
log = renamevars(log, "CH14", "CH14");
log = renamevars(log, "CH15", "CH15");
log = renamevars(log, "CH16", "CH16");
log = renamevars(log, "CH17", "CH17");
log = renamevars(log, "CH18", "CH18");
log = renamevars(log, "CH19", "CH19");
log = renamevars(log, "CH20", "CH20");

channel_names  = log.Properties.VariableNames;