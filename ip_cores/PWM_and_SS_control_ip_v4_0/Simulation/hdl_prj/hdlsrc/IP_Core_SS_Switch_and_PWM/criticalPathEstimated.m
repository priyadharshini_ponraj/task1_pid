open_system('gm_IP_Core_SS_Switch_and_PWM');
cs.HiliteType = 'user2';
cs.ForegroundColor = 'black';
cs.BackgroundColor = 'lightblue';
set_param(0, 'HiliteAncestorsData', cs);
hilite_system('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM', 'user2');
annotate_port('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM', 0, 1, 'cp : 6.863 ns');
cs.HiliteType = 'user2';
cs.ForegroundColor = 'black';
cs.BackgroundColor = 'lightblue';
set_param(0, 'HiliteAncestorsData', cs);
hilite_system('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/LimitPulseWidth', 'user2');
annotate_port('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/LimitPulseWidth', 0, 1, 'cp : 5.28 ns');
cs.HiliteType = 'user1';
cs.ForegroundColor = 'black';
cs.BackgroundColor = 'blue';
set_param(0, 'HiliteAncestorsData', cs);
hilite_system('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/out_0_pipe', 'user1');
annotate_port('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/out_0_pipe', 1, 1, 'cp : 6.863 ns');
cs.HiliteType = 'user2';
cs.ForegroundColor = 'black';
cs.BackgroundColor = 'lightblue';
set_param(0, 'HiliteAncestorsData', cs);
hilite_system('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/t', 'user2');
annotate_port('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/t', 0, 1, 'cp : 6.863 ns');
cs.HiliteType = 'user2';
cs.ForegroundColor = 'black';
cs.BackgroundColor = 'lightblue';
set_param(0, 'HiliteAncestorsData', cs);
hilite_system('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/MinOrMax', 'user2');
annotate_port('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/MinOrMax', 0, 1, 'cp : 2.752 ns');
cs.HiliteType = 'user2';
cs.ForegroundColor = 'black';
cs.BackgroundColor = 'lightblue';
set_param(0, 'HiliteAncestorsData', cs);
hilite_system('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/Relational Operator', 'user2');
annotate_port('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/Relational Operator', 0, 1, 'cp : 6.121 ns');
cs.HiliteType = 'user2';
cs.ForegroundColor = 'black';
cs.BackgroundColor = 'lightblue';
set_param(0, 'HiliteAncestorsData', cs);
hilite_system('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/Switch3', 'user2');
annotate_port('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/Switch3', 0, 1, 'cp : 6.863 ns');
cs.HiliteType = 'user2';
cs.ForegroundColor = 'black';
cs.BackgroundColor = 'lightblue';
set_param(0, 'HiliteAncestorsData', cs);
hilite_system('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/Switch7', 'user2');
annotate_port('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/Switch7', 0, 1, 'cp : 6.473 ns');
cs.HiliteType = 'user1';
cs.ForegroundColor = 'black';
cs.BackgroundColor = 'blue';
set_param(0, 'HiliteAncestorsData', cs);
hilite_system('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/HDL Counter2', 'user1');
annotate_port('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/HDL Counter2', 0, 1, 'cp : 0.337 ns');
cs.HiliteType = 'user2';
cs.ForegroundColor = 'black';
cs.BackgroundColor = 'lightblue';
set_param(0, 'HiliteAncestorsData', cs);
hilite_system('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/Switch10', 'user2');
annotate_port('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/Switch10', 0, 1, 'cp : 0.727 ns');
cs.HiliteType = 'user2';
cs.ForegroundColor = 'black';
cs.BackgroundColor = 'lightblue';
set_param(0, 'HiliteAncestorsData', cs);
hilite_system('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/Counter Ctrl', 'user2');
annotate_port('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/Counter Ctrl', 0, 3, 'cp : 1.759 ns');
cs.HiliteType = 'user2';
cs.ForegroundColor = 'black';
cs.BackgroundColor = 'lightblue';
set_param(0, 'HiliteAncestorsData', cs);
hilite_system('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/Switch', 'user2');
annotate_port('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/Switch', 0, 1, 'cp : 3.104 ns');
cs.HiliteType = 'user2';
cs.ForegroundColor = 'black';
cs.BackgroundColor = 'lightblue';
set_param(0, 'HiliteAncestorsData', cs);
hilite_system('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/Counter Ctrl_out3', 'user2');
annotate_port('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/Counter Ctrl_out3', 0, 1, 'cp : 1.759 ns');
cs.HiliteType = 'user2';
cs.ForegroundColor = 'black';
cs.BackgroundColor = 'lightblue';
set_param(0, 'HiliteAncestorsData', cs);
hilite_system('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/Counter Ctrl/Relational Operator1', 'user2');
annotate_port('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/Counter Ctrl/Relational Operator1', 0, 1, 'cp : 1.759 ns');
cs.HiliteType = 'user2';
cs.ForegroundColor = 'black';
cs.BackgroundColor = 'lightblue';
set_param(0, 'HiliteAncestorsData', cs);
hilite_system('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/LimitPulseWidth/Logical Operator', 'user2');
annotate_port('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/LimitPulseWidth/Logical Operator', 0, 1, 'cp : 4.928 ns');
cs.HiliteType = 'user2';
cs.ForegroundColor = 'black';
cs.BackgroundColor = 'lightblue';
set_param(0, 'HiliteAncestorsData', cs);
hilite_system('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/LimitPulseWidth/Relational Operator', 'user2');
annotate_port('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/LimitPulseWidth/Relational Operator', 0, 1, 'cp : 3.935 ns');
cs.HiliteType = 'user2';
cs.ForegroundColor = 'black';
cs.BackgroundColor = 'lightblue';
set_param(0, 'HiliteAncestorsData', cs);
hilite_system('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/LimitPulseWidth/Switch6', 'user2');
annotate_port('gm_IP_Core_SS_Switch_and_PWM/PWM_and_Switching_Signal_Control/GenPWM/LimitPulseWidth/Switch6', 0, 1, 'cp : 5.28 ns');
