function RTW_Sid2UrlHash() {
	this.urlHashMap = new Array();
	/* <S2>/Mode_AXI */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:194"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:194";
	/* <S2>/GenPWM */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:371"] = "PWM_and_SS_control_V4_ip_src_PWM_and_Switching_Signal_Control.vhd:222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250";
	/* <S2>/Switch7 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:221"] = "PWM_and_SS_control_V4_ip_src_PWM_and_Switching_Signal_Control.vhd:287,288";
	/* <S2>/Switch8 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:222"] = "PWM_and_SS_control_V4_ip_src_PWM_and_Switching_Signal_Control.vhd:295,296";
	/* <S2>/Switch9 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:223"] = "PWM_and_SS_control_V4_ip_src_PWM_and_Switching_Signal_Control.vhd:303,304";
	/* <S2>/VSI Control Signal Switch */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:8"] = "PWM_and_SS_control_V4_ip_src_PWM_and_Switching_Signal_Control.vhd:252,253,254,255,256,257,258,259,260,261,262,263,264,265,266,267,268,269,270,271,272,273,274,275,276,277,278";
	/* <S8>/PWM_en_AXI */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:372"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:372";
	/* <S8>/f_carrier_kHz_AXI */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:373"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:373";
	/* <S8>/T_carrier_us_AXI */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:374"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:374";
	/* <S8>/min_pulse_width_AXI */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:375"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:375";
	/* <S8>/Constant */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:379"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:677";
	/* <S8>/Constant1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:380"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:405,406";
	/* <S8>/Constant10 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:381"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:264,265";
	/* <S8>/Constant11 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:382"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:679";
	/* <S8>/Constant2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:383"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:408,409";
	/* <S8>/Constant4 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:384"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:493,494";
	/* <S8>/Constant5 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:385"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:496,497";
	/* <S8>/Constant7 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:386"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:581,582";
	/* <S8>/Constant8 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:387"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:584,585";
	/* <S8>/Counter Ctrl */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:388"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:217,218,219,220,221,222,223,224,225,226,227,228";
	/* <S8>/Delay */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:417"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:363,364,365,366,367,368,369,370,371,372";
	/* <S8>/Delay1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:418"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:465,466,467,468,469,470,471,472,473,474";
	/* <S8>/Delay2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:419"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:553,554,555,556,557,558,559,560,561,562";
	/* <S8>/Demux */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:420"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:419";
	/* <S8>/Demux1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:421"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:507";
	/* <S8>/Demux2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:422"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:595";
	/* <S8>/HDL Counter2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:324,325,326,327,328,329,330,331,332,333,335,337,338,339";
	/* <S8>/LimitPulseWidth */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:426"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:230,231,232,233,234";
	/* <S8>/LimitPulseWidth1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:441"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:236,237,238,239,240";
	/* <S8>/LimitPulseWidth2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:456"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:242,243,244,245,246";
	/* <S8>/MinOrMax */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:550"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:347";
	/* <S8>/Relational
Operator */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:471"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:395,396,398,399";
	/* <S8>/Relational
Operator1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:472"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:483,484,486,487";
	/* <S8>/Relational
Operator2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:473"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:571,572,574,575";
	/* <S8>/Scope */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:475"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:475";
	/* <S8>/Scope1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:599"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:599";
	/* <S8>/Scope10 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:476"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:476";
	/* <S8>/Scope2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:477"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:477";
	/* <S8>/Scope4 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:479"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:479";
	/* <S8>/Switch */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:480"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:376,377";
	/* <S8>/Switch1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:481"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:478,479";
	/* <S8>/Switch10 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:563"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:342,343";
	/* <S8>/Switch2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:482"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:566,567";
	/* <S8>/Switch3 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:483"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:416,417";
	/* <S8>/Switch4 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:484"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:504,505";
	/* <S8>/Switch5 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:485"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:592,593";
	/* <S8>/Switch6 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:486"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:682,683";
	/* <S8>/Switch7 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:487"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:412,413";
	/* <S8>/Switch8 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:488"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:500,501";
	/* <S8>/Switch9 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:489"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:588,589";
	/* <S9>/Constant */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:531"] = "PWM_and_SS_control_V4_ip_src_VSI_Control_Signal_Switch.vhd:155";
	/* <S9>/Switch */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:1"] = "PWM_and_SS_control_V4_ip_src_VSI_Control_Signal_Switch.vhd:152,153";
	/* <S9>/Switch1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:2"] = "PWM_and_SS_control_V4_ip_src_VSI_Control_Signal_Switch.vhd:180,181";
	/* <S9>/Switch10 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:529"] = "PWM_and_SS_control_V4_ip_src_VSI_Control_Signal_Switch.vhd:294,295";
	/* <S9>/Switch11 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:530"] = "PWM_and_SS_control_V4_ip_src_VSI_Control_Signal_Switch.vhd:320,321";
	/* <S9>/Switch2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3"] = "PWM_and_SS_control_V4_ip_src_VSI_Control_Signal_Switch.vhd:222,223";
	/* <S9>/Switch3 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:4"] = "PWM_and_SS_control_V4_ip_src_VSI_Control_Signal_Switch.vhd:248,249";
	/* <S9>/Switch4 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:5"] = "PWM_and_SS_control_V4_ip_src_VSI_Control_Signal_Switch.vhd:290,291";
	/* <S9>/Switch5 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:6"] = "PWM_and_SS_control_V4_ip_src_VSI_Control_Signal_Switch.vhd:316,317";
	/* <S9>/Switch6 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:525"] = "PWM_and_SS_control_V4_ip_src_VSI_Control_Signal_Switch.vhd:158,159";
	/* <S9>/Switch7 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:526"] = "PWM_and_SS_control_V4_ip_src_VSI_Control_Signal_Switch.vhd:184,185";
	/* <S9>/Switch8 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:527"] = "PWM_and_SS_control_V4_ip_src_VSI_Control_Signal_Switch.vhd:226,227";
	/* <S9>/Switch9 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:528"] = "PWM_and_SS_control_V4_ip_src_VSI_Control_Signal_Switch.vhd:252,253";
	/* <S10>/Constant1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:542"] = "PWM_and_SS_control_V4_ip_src_Counter_Ctrl.vhd:142";
	/* <S10>/Constant11 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:392"] = "PWM_and_SS_control_V4_ip_src_Counter_Ctrl.vhd:79";
	/* <S10>/Constant12 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:393"] = "PWM_and_SS_control_V4_ip_src_Counter_Ctrl.vhd:97";
	/* <S10>/Constant14 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:394"] = "PWM_and_SS_control_V4_ip_src_Counter_Ctrl.vhd:95";
	/* <S10>/Delay */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:395"] = "PWM_and_SS_control_V4_ip_src_Counter_Ctrl.vhd:121,122,123,124,125,126,127,128,129,130";
	/* <S10>/Delay6 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:396"] = "PWM_and_SS_control_V4_ip_src_Counter_Ctrl.vhd:103,104,105,106,107,108,109,110,111,112";
	/* <S10>/Logical
Operator */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:399"] = "PWM_and_SS_control_V4_ip_src_Counter_Ctrl.vhd:85";
	/* <S10>/Product */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:400"] = "PWM_and_SS_control_V4_ip_src_Counter_Ctrl.vhd:133,134";
	/* <S10>/Relational
Operator1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:510"] = "PWM_and_SS_control_V4_ip_src_Counter_Ctrl.vhd:139,140";
	/* <S10>/Relational
Operator2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:541"] = "PWM_and_SS_control_V4_ip_src_Counter_Ctrl.vhd:145,146";
	/* <S10>/Relational
Operator4 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:401"] = "PWM_and_SS_control_V4_ip_src_Counter_Ctrl.vhd:76,77";
	/* <S10>/Relational
Operator5 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:402"] = "PWM_and_SS_control_V4_ip_src_Counter_Ctrl.vhd:82,83";
	/* <S10>/Scope9 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:407"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:407";
	/* <S10>/Switch6 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:408"] = "PWM_and_SS_control_V4_ip_src_Counter_Ctrl.vhd:116,117";
	/* <S10>/Switch7 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:409"] = "PWM_and_SS_control_V4_ip_src_Counter_Ctrl.vhd:100,101";
	/* <S11>/Constant */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:212"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:212";
	/* <S11>/Count_reg */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:9"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:9";
	/* <S11>/DT_convert */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:10"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:10";
	/* <S11>/DT_convert1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:11"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:11";
	/* <S11>/Free_running_or_modulo */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:17"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:17";
	/* <S11>/From_value */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:19"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:19";
	/* <S11>/Init_value */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:18"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:18";
	/* <S11>/Relational
Operator */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:211"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:211";
	/* <S11>/Step_value */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:21"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:21";
	/* <S11>/Switch_dir */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:30"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:30";
	/* <S11>/Switch_enb */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:31"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:31";
	/* <S11>/Switch_load */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:32"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:32";
	/* <S11>/Switch_max */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:33"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:33";
	/* <S11>/Switch_reset */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:34"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:34";
	/* <S11>/Switch_type */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:35"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:35";
	/* <S11>/const_load */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:38"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:38";
	/* <S11>/const_load_val */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:39"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:39";
	/* <S11>/const_rst */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:40"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:40";
	/* <S12>/Add */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:429"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth.vhd:60";
	/* <S12>/Constant */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:430"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth.vhd:56";
	/* <S12>/Constant1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:431"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth.vhd:87";
	/* <S12>/Constant2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:432"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth.vhd:91";
	/* <S12>/Logical
Operator */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:433"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth.vhd:77";
	/* <S12>/Relational
Operator */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:434"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth.vhd:62,64,65";
	/* <S12>/Relational
Operator1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:435"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth.vhd:72,74,75";
	/* <S12>/Saturation */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:436"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth.vhd:68,69,70";
	/* <S12>/Switch1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:437"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth.vhd:94,95";
	/* <S12>/Switch6 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:438"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth.vhd:98,99";
	/* <S13>/Add */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:444"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth1.vhd:60";
	/* <S13>/Constant */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:445"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth1.vhd:56";
	/* <S13>/Constant1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:446"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth1.vhd:87";
	/* <S13>/Constant2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:447"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth1.vhd:91";
	/* <S13>/Logical
Operator */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:448"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth1.vhd:77";
	/* <S13>/Relational
Operator */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:449"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth1.vhd:62,64,65";
	/* <S13>/Relational
Operator1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:450"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth1.vhd:72,74,75";
	/* <S13>/Saturation */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:451"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth1.vhd:68,69,70";
	/* <S13>/Switch1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:452"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth1.vhd:94,95";
	/* <S13>/Switch6 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:453"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth1.vhd:98,99";
	/* <S14>/Add */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:459"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth2.vhd:60";
	/* <S14>/Constant */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:460"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth2.vhd:56";
	/* <S14>/Constant1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:461"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth2.vhd:87";
	/* <S14>/Constant2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:462"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth2.vhd:91";
	/* <S14>/Logical
Operator */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:463"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth2.vhd:77";
	/* <S14>/Relational
Operator */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:464"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth2.vhd:62,64,65";
	/* <S14>/Relational
Operator1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:465"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth2.vhd:72,74,75";
	/* <S14>/Saturation */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:466"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth2.vhd:68,69,70";
	/* <S14>/Switch1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:467"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth2.vhd:94,95";
	/* <S14>/Switch6 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:468"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth2.vhd:98,99";
	/* <S15>/Add */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:4"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:4";
	/* <S15>/Mod_value */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:5"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:5";
	/* <S15>/Switch_wrap */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:6"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:6";
	/* <S15>/Wrap */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:7"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:7";
	/* <S16>/Logical
Operator */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:14"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:14";
	/* <S16>/Pos_step */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:15"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:15";
	/* <S17>/Add */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:25"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:25";
	/* <S17>/Mod_value */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:26"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:26";
	/* <S17>/Switch_wrap */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:27"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:27";
	/* <S17>/Wrap */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:28"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:28";
	/* <S18>/Compare */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:215:2"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:215:2";
	/* <S18>/Constant */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:215:3"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:215:3";
	/* <S19>/Compare */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:213:2"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:213:2";
	/* <S19>/Constant */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:213:3"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:213:3";
	this.getUrlHash = function(sid) { return this.urlHashMap[sid];}
}
RTW_Sid2UrlHash.instance = new RTW_Sid2UrlHash();
function RTW_rtwnameSIDMap() {
	this.rtwnameHashMap = new Array();
	this.sidHashMap = new Array();
	this.rtwnameHashMap["<Root>"] = {sid: "IP_Core_SS_Switch_and_PWM"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM"] = {rtwname: "<Root>"};
	this.rtwnameHashMap["<S2>/PWM_en_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:193"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:193"] = {rtwname: "<S2>/PWM_en_AXI"};
	this.rtwnameHashMap["<S2>/Mode_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:194"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:194"] = {rtwname: "<S2>/Mode_AXI"};
	this.rtwnameHashMap["<S2>/Scal_f_carrier_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:195"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:195"] = {rtwname: "<S2>/Scal_f_carrier_AXI"};
	this.rtwnameHashMap["<S2>/Scal_T_carrier_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:502"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:502"] = {rtwname: "<S2>/Scal_T_carrier_AXI"};
	this.rtwnameHashMap["<S2>/PWM_min_pulse_width_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:196"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:196"] = {rtwname: "<S2>/PWM_min_pulse_width_AXI"};
	this.rtwnameHashMap["<S2>/m_u1_norm"] = {sid: "IP_Core_SS_Switch_and_PWM:197"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:197"] = {rtwname: "<S2>/m_u1_norm"};
	this.rtwnameHashMap["<S2>/m_u2_norm"] = {sid: "IP_Core_SS_Switch_and_PWM:198"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:198"] = {rtwname: "<S2>/m_u2_norm"};
	this.rtwnameHashMap["<S2>/m_u3_norm"] = {sid: "IP_Core_SS_Switch_and_PWM:199"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:199"] = {rtwname: "<S2>/m_u3_norm"};
	this.rtwnameHashMap["<S2>/m_u1_norm_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:201"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:201"] = {rtwname: "<S2>/m_u1_norm_AXI"};
	this.rtwnameHashMap["<S2>/m_u2_norm_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:203"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:203"] = {rtwname: "<S2>/m_u2_norm_AXI"};
	this.rtwnameHashMap["<S2>/m_u3_norm_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:205"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:205"] = {rtwname: "<S2>/m_u3_norm_AXI"};
	this.rtwnameHashMap["<S2>/SS0_IN_External"] = {sid: "IP_Core_SS_Switch_and_PWM:200"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:200"] = {rtwname: "<S2>/SS0_IN_External"};
	this.rtwnameHashMap["<S2>/SS1_IN_External"] = {sid: "IP_Core_SS_Switch_and_PWM:202"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:202"] = {rtwname: "<S2>/SS1_IN_External"};
	this.rtwnameHashMap["<S2>/SS2_IN_External"] = {sid: "IP_Core_SS_Switch_and_PWM:204"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:204"] = {rtwname: "<S2>/SS2_IN_External"};
	this.rtwnameHashMap["<S2>/SS3_IN_External"] = {sid: "IP_Core_SS_Switch_and_PWM:206"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:206"] = {rtwname: "<S2>/SS3_IN_External"};
	this.rtwnameHashMap["<S2>/SS4_IN_External"] = {sid: "IP_Core_SS_Switch_and_PWM:207"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:207"] = {rtwname: "<S2>/SS4_IN_External"};
	this.rtwnameHashMap["<S2>/SS5_IN_External"] = {sid: "IP_Core_SS_Switch_and_PWM:208"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:208"] = {rtwname: "<S2>/SS5_IN_External"};
	this.rtwnameHashMap["<S2>/TriState_HB1_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:514"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:514"] = {rtwname: "<S2>/TriState_HB1_AXI"};
	this.rtwnameHashMap["<S2>/TriState_HB2_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:515"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:515"] = {rtwname: "<S2>/TriState_HB2_AXI"};
	this.rtwnameHashMap["<S2>/TriState_HB3_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:516"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:516"] = {rtwname: "<S2>/TriState_HB3_AXI"};
	this.rtwnameHashMap["<S2>/count_in"] = {sid: "IP_Core_SS_Switch_and_PWM:566"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:566"] = {rtwname: "<S2>/count_in"};
	this.rtwnameHashMap["<S2>/count_src_ext_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:605"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:605"] = {rtwname: "<S2>/count_src_ext_AXI"};
	this.rtwnameHashMap["<S2>/GenPWM"] = {sid: "IP_Core_SS_Switch_and_PWM:371"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:371"] = {rtwname: "<S2>/GenPWM"};
	this.rtwnameHashMap["<S2>/Switch7"] = {sid: "IP_Core_SS_Switch_and_PWM:221"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:221"] = {rtwname: "<S2>/Switch7"};
	this.rtwnameHashMap["<S2>/Switch8"] = {sid: "IP_Core_SS_Switch_and_PWM:222"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:222"] = {rtwname: "<S2>/Switch8"};
	this.rtwnameHashMap["<S2>/Switch9"] = {sid: "IP_Core_SS_Switch_and_PWM:223"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:223"] = {rtwname: "<S2>/Switch9"};
	this.rtwnameHashMap["<S2>/VSI Control Signal Switch"] = {sid: "IP_Core_SS_Switch_and_PWM:8"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:8"] = {rtwname: "<S2>/VSI Control Signal Switch"};
	this.rtwnameHashMap["<S2>/SS0_OUT"] = {sid: "IP_Core_SS_Switch_and_PWM:209"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:209"] = {rtwname: "<S2>/SS0_OUT"};
	this.rtwnameHashMap["<S2>/SS1_OUT"] = {sid: "IP_Core_SS_Switch_and_PWM:210"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:210"] = {rtwname: "<S2>/SS1_OUT"};
	this.rtwnameHashMap["<S2>/SS2_OUT"] = {sid: "IP_Core_SS_Switch_and_PWM:211"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:211"] = {rtwname: "<S2>/SS2_OUT"};
	this.rtwnameHashMap["<S2>/SS3_OUT"] = {sid: "IP_Core_SS_Switch_and_PWM:212"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:212"] = {rtwname: "<S2>/SS3_OUT"};
	this.rtwnameHashMap["<S2>/SS4_OUT"] = {sid: "IP_Core_SS_Switch_and_PWM:213"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:213"] = {rtwname: "<S2>/SS4_OUT"};
	this.rtwnameHashMap["<S2>/SS5_OUT"] = {sid: "IP_Core_SS_Switch_and_PWM:214"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:214"] = {rtwname: "<S2>/SS5_OUT"};
	this.rtwnameHashMap["<S2>/PWM_en_rd_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:215"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:215"] = {rtwname: "<S2>/PWM_en_rd_AXI"};
	this.rtwnameHashMap["<S2>/PWM_f_carrier_kHz_rd_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:216"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:216"] = {rtwname: "<S2>/PWM_f_carrier_kHz_rd_AXI"};
	this.rtwnameHashMap["<S2>/PWM_T_carrier_us_rd_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:501"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:501"] = {rtwname: "<S2>/PWM_T_carrier_us_rd_AXI"};
	this.rtwnameHashMap["<S2>/PWM_min_pulse_width_rd_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:217"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:217"] = {rtwname: "<S2>/PWM_min_pulse_width_rd_AXI"};
	this.rtwnameHashMap["<S2>/PWM_enb_out"] = {sid: "IP_Core_SS_Switch_and_PWM:218"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:218"] = {rtwname: "<S2>/PWM_enb_out"};
	this.rtwnameHashMap["<S2>/Mode_rd_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:220"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:220"] = {rtwname: "<S2>/Mode_rd_AXI"};
	this.rtwnameHashMap["<S2>/Triangular_Max"] = {sid: "IP_Core_SS_Switch_and_PWM:512"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:512"] = {rtwname: "<S2>/Triangular_Max"};
	this.rtwnameHashMap["<S2>/Triangular_Min"] = {sid: "IP_Core_SS_Switch_and_PWM:538"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:538"] = {rtwname: "<S2>/Triangular_Min"};
	this.rtwnameHashMap["<S2>/count_out"] = {sid: "IP_Core_SS_Switch_and_PWM:567"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:567"] = {rtwname: "<S2>/count_out"};
	this.rtwnameHashMap["<S2>/dir_out"] = {sid: "IP_Core_SS_Switch_and_PWM:598"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:598"] = {rtwname: "<S2>/dir_out"};
	this.rtwnameHashMap["<S8>/PWM_en_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:372"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:372"] = {rtwname: "<S8>/PWM_en_AXI"};
	this.rtwnameHashMap["<S8>/f_carrier_kHz_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:373"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:373"] = {rtwname: "<S8>/f_carrier_kHz_AXI"};
	this.rtwnameHashMap["<S8>/T_carrier_us_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:374"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:374"] = {rtwname: "<S8>/T_carrier_us_AXI"};
	this.rtwnameHashMap["<S8>/min_pulse_width_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:375"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:375"] = {rtwname: "<S8>/min_pulse_width_AXI"};
	this.rtwnameHashMap["<S8>/U1_norm"] = {sid: "IP_Core_SS_Switch_and_PWM:376"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:376"] = {rtwname: "<S8>/U1_norm"};
	this.rtwnameHashMap["<S8>/U2_norm"] = {sid: "IP_Core_SS_Switch_and_PWM:377"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:377"] = {rtwname: "<S8>/U2_norm"};
	this.rtwnameHashMap["<S8>/U3_norm"] = {sid: "IP_Core_SS_Switch_and_PWM:378"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:378"] = {rtwname: "<S8>/U3_norm"};
	this.rtwnameHashMap["<S8>/count_in"] = {sid: "IP_Core_SS_Switch_and_PWM:564"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:564"] = {rtwname: "<S8>/count_in"};
	this.rtwnameHashMap["<S8>/count_src_ext_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:604"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:604"] = {rtwname: "<S8>/count_src_ext_AXI"};
	this.rtwnameHashMap["<S8>/Constant"] = {sid: "IP_Core_SS_Switch_and_PWM:379"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:379"] = {rtwname: "<S8>/Constant"};
	this.rtwnameHashMap["<S8>/Constant1"] = {sid: "IP_Core_SS_Switch_and_PWM:380"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:380"] = {rtwname: "<S8>/Constant1"};
	this.rtwnameHashMap["<S8>/Constant10"] = {sid: "IP_Core_SS_Switch_and_PWM:381"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:381"] = {rtwname: "<S8>/Constant10"};
	this.rtwnameHashMap["<S8>/Constant11"] = {sid: "IP_Core_SS_Switch_and_PWM:382"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:382"] = {rtwname: "<S8>/Constant11"};
	this.rtwnameHashMap["<S8>/Constant2"] = {sid: "IP_Core_SS_Switch_and_PWM:383"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:383"] = {rtwname: "<S8>/Constant2"};
	this.rtwnameHashMap["<S8>/Constant4"] = {sid: "IP_Core_SS_Switch_and_PWM:384"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:384"] = {rtwname: "<S8>/Constant4"};
	this.rtwnameHashMap["<S8>/Constant5"] = {sid: "IP_Core_SS_Switch_and_PWM:385"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:385"] = {rtwname: "<S8>/Constant5"};
	this.rtwnameHashMap["<S8>/Constant7"] = {sid: "IP_Core_SS_Switch_and_PWM:386"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:386"] = {rtwname: "<S8>/Constant7"};
	this.rtwnameHashMap["<S8>/Constant8"] = {sid: "IP_Core_SS_Switch_and_PWM:387"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:387"] = {rtwname: "<S8>/Constant8"};
	this.rtwnameHashMap["<S8>/Counter Ctrl"] = {sid: "IP_Core_SS_Switch_and_PWM:388"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:388"] = {rtwname: "<S8>/Counter Ctrl"};
	this.rtwnameHashMap["<S8>/Data Type Conversion1"] = {sid: "IP_Core_SS_Switch_and_PWM:416"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:416"] = {rtwname: "<S8>/Data Type Conversion1"};
	this.rtwnameHashMap["<S8>/Delay"] = {sid: "IP_Core_SS_Switch_and_PWM:417"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:417"] = {rtwname: "<S8>/Delay"};
	this.rtwnameHashMap["<S8>/Delay1"] = {sid: "IP_Core_SS_Switch_and_PWM:418"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:418"] = {rtwname: "<S8>/Delay1"};
	this.rtwnameHashMap["<S8>/Delay2"] = {sid: "IP_Core_SS_Switch_and_PWM:419"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:419"] = {rtwname: "<S8>/Delay2"};
	this.rtwnameHashMap["<S8>/Demux"] = {sid: "IP_Core_SS_Switch_and_PWM:420"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:420"] = {rtwname: "<S8>/Demux"};
	this.rtwnameHashMap["<S8>/Demux1"] = {sid: "IP_Core_SS_Switch_and_PWM:421"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:421"] = {rtwname: "<S8>/Demux1"};
	this.rtwnameHashMap["<S8>/Demux2"] = {sid: "IP_Core_SS_Switch_and_PWM:422"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:422"] = {rtwname: "<S8>/Demux2"};
	this.rtwnameHashMap["<S8>/HDL Counter1"] = {sid: "IP_Core_SS_Switch_and_PWM:424"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:424"] = {rtwname: "<S8>/HDL Counter1"};
	this.rtwnameHashMap["<S8>/HDL Counter2"] = {sid: "IP_Core_SS_Switch_and_PWM:425"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425"] = {rtwname: "<S8>/HDL Counter2"};
	this.rtwnameHashMap["<S8>/LimitPulseWidth"] = {sid: "IP_Core_SS_Switch_and_PWM:426"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:426"] = {rtwname: "<S8>/LimitPulseWidth"};
	this.rtwnameHashMap["<S8>/LimitPulseWidth1"] = {sid: "IP_Core_SS_Switch_and_PWM:441"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:441"] = {rtwname: "<S8>/LimitPulseWidth1"};
	this.rtwnameHashMap["<S8>/LimitPulseWidth2"] = {sid: "IP_Core_SS_Switch_and_PWM:456"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:456"] = {rtwname: "<S8>/LimitPulseWidth2"};
	this.rtwnameHashMap["<S8>/MinOrMax"] = {sid: "IP_Core_SS_Switch_and_PWM:550"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:550"] = {rtwname: "<S8>/MinOrMax"};
	this.rtwnameHashMap["<S8>/Relational Operator"] = {sid: "IP_Core_SS_Switch_and_PWM:471"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:471"] = {rtwname: "<S8>/Relational Operator"};
	this.rtwnameHashMap["<S8>/Relational Operator1"] = {sid: "IP_Core_SS_Switch_and_PWM:472"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:472"] = {rtwname: "<S8>/Relational Operator1"};
	this.rtwnameHashMap["<S8>/Relational Operator2"] = {sid: "IP_Core_SS_Switch_and_PWM:473"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:473"] = {rtwname: "<S8>/Relational Operator2"};
	this.rtwnameHashMap["<S8>/Relay1"] = {sid: "IP_Core_SS_Switch_and_PWM:474"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:474"] = {rtwname: "<S8>/Relay1"};
	this.rtwnameHashMap["<S8>/Scope"] = {sid: "IP_Core_SS_Switch_and_PWM:475"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:475"] = {rtwname: "<S8>/Scope"};
	this.rtwnameHashMap["<S8>/Scope1"] = {sid: "IP_Core_SS_Switch_and_PWM:599"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:599"] = {rtwname: "<S8>/Scope1"};
	this.rtwnameHashMap["<S8>/Scope10"] = {sid: "IP_Core_SS_Switch_and_PWM:476"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:476"] = {rtwname: "<S8>/Scope10"};
	this.rtwnameHashMap["<S8>/Scope2"] = {sid: "IP_Core_SS_Switch_and_PWM:477"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:477"] = {rtwname: "<S8>/Scope2"};
	this.rtwnameHashMap["<S8>/Scope4"] = {sid: "IP_Core_SS_Switch_and_PWM:479"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:479"] = {rtwname: "<S8>/Scope4"};
	this.rtwnameHashMap["<S8>/Switch"] = {sid: "IP_Core_SS_Switch_and_PWM:480"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:480"] = {rtwname: "<S8>/Switch"};
	this.rtwnameHashMap["<S8>/Switch1"] = {sid: "IP_Core_SS_Switch_and_PWM:481"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:481"] = {rtwname: "<S8>/Switch1"};
	this.rtwnameHashMap["<S8>/Switch10"] = {sid: "IP_Core_SS_Switch_and_PWM:563"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:563"] = {rtwname: "<S8>/Switch10"};
	this.rtwnameHashMap["<S8>/Switch2"] = {sid: "IP_Core_SS_Switch_and_PWM:482"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:482"] = {rtwname: "<S8>/Switch2"};
	this.rtwnameHashMap["<S8>/Switch3"] = {sid: "IP_Core_SS_Switch_and_PWM:483"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:483"] = {rtwname: "<S8>/Switch3"};
	this.rtwnameHashMap["<S8>/Switch4"] = {sid: "IP_Core_SS_Switch_and_PWM:484"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:484"] = {rtwname: "<S8>/Switch4"};
	this.rtwnameHashMap["<S8>/Switch5"] = {sid: "IP_Core_SS_Switch_and_PWM:485"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:485"] = {rtwname: "<S8>/Switch5"};
	this.rtwnameHashMap["<S8>/Switch6"] = {sid: "IP_Core_SS_Switch_and_PWM:486"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:486"] = {rtwname: "<S8>/Switch6"};
	this.rtwnameHashMap["<S8>/Switch7"] = {sid: "IP_Core_SS_Switch_and_PWM:487"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:487"] = {rtwname: "<S8>/Switch7"};
	this.rtwnameHashMap["<S8>/Switch8"] = {sid: "IP_Core_SS_Switch_and_PWM:488"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:488"] = {rtwname: "<S8>/Switch8"};
	this.rtwnameHashMap["<S8>/Switch9"] = {sid: "IP_Core_SS_Switch_and_PWM:489"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:489"] = {rtwname: "<S8>/Switch9"};
	this.rtwnameHashMap["<S8>/S1"] = {sid: "IP_Core_SS_Switch_and_PWM:490"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:490"] = {rtwname: "<S8>/S1"};
	this.rtwnameHashMap["<S8>/S2"] = {sid: "IP_Core_SS_Switch_and_PWM:491"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:491"] = {rtwname: "<S8>/S2"};
	this.rtwnameHashMap["<S8>/S3"] = {sid: "IP_Core_SS_Switch_and_PWM:492"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:492"] = {rtwname: "<S8>/S3"};
	this.rtwnameHashMap["<S8>/S4"] = {sid: "IP_Core_SS_Switch_and_PWM:493"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:493"] = {rtwname: "<S8>/S4"};
	this.rtwnameHashMap["<S8>/S5"] = {sid: "IP_Core_SS_Switch_and_PWM:494"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:494"] = {rtwname: "<S8>/S5"};
	this.rtwnameHashMap["<S8>/S6"] = {sid: "IP_Core_SS_Switch_and_PWM:495"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:495"] = {rtwname: "<S8>/S6"};
	this.rtwnameHashMap["<S8>/PWM_en_rd_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:496"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:496"] = {rtwname: "<S8>/PWM_en_rd_AXI"};
	this.rtwnameHashMap["<S8>/f_carrier_kHz_rd_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:497"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:497"] = {rtwname: "<S8>/f_carrier_kHz_rd_AXI"};
	this.rtwnameHashMap["<S8>/T_carrier_us_rd_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:498"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:498"] = {rtwname: "<S8>/T_carrier_us_rd_AXI"};
	this.rtwnameHashMap["<S8>/min_pulse_width_rd_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:499"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:499"] = {rtwname: "<S8>/min_pulse_width_rd_AXI"};
	this.rtwnameHashMap["<S8>/enb_out"] = {sid: "IP_Core_SS_Switch_and_PWM:500"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:500"] = {rtwname: "<S8>/enb_out"};
	this.rtwnameHashMap["<S8>/Triangle_Max"] = {sid: "IP_Core_SS_Switch_and_PWM:511"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:511"] = {rtwname: "<S8>/Triangle_Max"};
	this.rtwnameHashMap["<S8>/Triangle_Min"] = {sid: "IP_Core_SS_Switch_and_PWM:539"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:539"] = {rtwname: "<S8>/Triangle_Min"};
	this.rtwnameHashMap["<S8>/count_out"] = {sid: "IP_Core_SS_Switch_and_PWM:561"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:561"] = {rtwname: "<S8>/count_out"};
	this.rtwnameHashMap["<S8>/dir_out"] = {sid: "IP_Core_SS_Switch_and_PWM:597"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:597"] = {rtwname: "<S8>/dir_out"};
	this.rtwnameHashMap["<S9>/Switch_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:9"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:9"] = {rtwname: "<S9>/Switch_AXI"};
	this.rtwnameHashMap["<S9>/SS0_IN_PWM"] = {sid: "IP_Core_SS_Switch_and_PWM:10"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:10"] = {rtwname: "<S9>/SS0_IN_PWM"};
	this.rtwnameHashMap["<S9>/SS1_IN_PWM"] = {sid: "IP_Core_SS_Switch_and_PWM:12"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:12"] = {rtwname: "<S9>/SS1_IN_PWM"};
	this.rtwnameHashMap["<S9>/SS2_IN_PWM"] = {sid: "IP_Core_SS_Switch_and_PWM:14"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:14"] = {rtwname: "<S9>/SS2_IN_PWM"};
	this.rtwnameHashMap["<S9>/SS3_IN_PWM"] = {sid: "IP_Core_SS_Switch_and_PWM:16"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:16"] = {rtwname: "<S9>/SS3_IN_PWM"};
	this.rtwnameHashMap["<S9>/SS4_IN_PWM"] = {sid: "IP_Core_SS_Switch_and_PWM:18"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:18"] = {rtwname: "<S9>/SS4_IN_PWM"};
	this.rtwnameHashMap["<S9>/SS5_IN_PWM"] = {sid: "IP_Core_SS_Switch_and_PWM:20"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:20"] = {rtwname: "<S9>/SS5_IN_PWM"};
	this.rtwnameHashMap["<S9>/SS0_IN_External"] = {sid: "IP_Core_SS_Switch_and_PWM:11"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:11"] = {rtwname: "<S9>/SS0_IN_External"};
	this.rtwnameHashMap["<S9>/SS1_IN_External"] = {sid: "IP_Core_SS_Switch_and_PWM:13"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:13"] = {rtwname: "<S9>/SS1_IN_External"};
	this.rtwnameHashMap["<S9>/SS2_IN_External"] = {sid: "IP_Core_SS_Switch_and_PWM:15"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:15"] = {rtwname: "<S9>/SS2_IN_External"};
	this.rtwnameHashMap["<S9>/SS3_IN_External"] = {sid: "IP_Core_SS_Switch_and_PWM:17"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:17"] = {rtwname: "<S9>/SS3_IN_External"};
	this.rtwnameHashMap["<S9>/SS4_IN_External"] = {sid: "IP_Core_SS_Switch_and_PWM:19"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:19"] = {rtwname: "<S9>/SS4_IN_External"};
	this.rtwnameHashMap["<S9>/SS5_IN_External"] = {sid: "IP_Core_SS_Switch_and_PWM:21"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:21"] = {rtwname: "<S9>/SS5_IN_External"};
	this.rtwnameHashMap["<S9>/TriState_HB1_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:518"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:518"] = {rtwname: "<S9>/TriState_HB1_AXI"};
	this.rtwnameHashMap["<S9>/TriState_HB2_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:519"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:519"] = {rtwname: "<S9>/TriState_HB2_AXI"};
	this.rtwnameHashMap["<S9>/TriState_HB3_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:520"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:520"] = {rtwname: "<S9>/TriState_HB3_AXI"};
	this.rtwnameHashMap["<S9>/Constant"] = {sid: "IP_Core_SS_Switch_and_PWM:531"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:531"] = {rtwname: "<S9>/Constant"};
	this.rtwnameHashMap["<S9>/Switch"] = {sid: "IP_Core_SS_Switch_and_PWM:1"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1"] = {rtwname: "<S9>/Switch"};
	this.rtwnameHashMap["<S9>/Switch1"] = {sid: "IP_Core_SS_Switch_and_PWM:2"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:2"] = {rtwname: "<S9>/Switch1"};
	this.rtwnameHashMap["<S9>/Switch10"] = {sid: "IP_Core_SS_Switch_and_PWM:529"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:529"] = {rtwname: "<S9>/Switch10"};
	this.rtwnameHashMap["<S9>/Switch11"] = {sid: "IP_Core_SS_Switch_and_PWM:530"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:530"] = {rtwname: "<S9>/Switch11"};
	this.rtwnameHashMap["<S9>/Switch2"] = {sid: "IP_Core_SS_Switch_and_PWM:3"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3"] = {rtwname: "<S9>/Switch2"};
	this.rtwnameHashMap["<S9>/Switch3"] = {sid: "IP_Core_SS_Switch_and_PWM:4"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:4"] = {rtwname: "<S9>/Switch3"};
	this.rtwnameHashMap["<S9>/Switch4"] = {sid: "IP_Core_SS_Switch_and_PWM:5"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:5"] = {rtwname: "<S9>/Switch4"};
	this.rtwnameHashMap["<S9>/Switch5"] = {sid: "IP_Core_SS_Switch_and_PWM:6"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:6"] = {rtwname: "<S9>/Switch5"};
	this.rtwnameHashMap["<S9>/Switch6"] = {sid: "IP_Core_SS_Switch_and_PWM:525"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:525"] = {rtwname: "<S9>/Switch6"};
	this.rtwnameHashMap["<S9>/Switch7"] = {sid: "IP_Core_SS_Switch_and_PWM:526"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:526"] = {rtwname: "<S9>/Switch7"};
	this.rtwnameHashMap["<S9>/Switch8"] = {sid: "IP_Core_SS_Switch_and_PWM:527"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:527"] = {rtwname: "<S9>/Switch8"};
	this.rtwnameHashMap["<S9>/Switch9"] = {sid: "IP_Core_SS_Switch_and_PWM:528"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:528"] = {rtwname: "<S9>/Switch9"};
	this.rtwnameHashMap["<S9>/SS0_OUT"] = {sid: "IP_Core_SS_Switch_and_PWM:22"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:22"] = {rtwname: "<S9>/SS0_OUT"};
	this.rtwnameHashMap["<S9>/SS1_OUT"] = {sid: "IP_Core_SS_Switch_and_PWM:23"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:23"] = {rtwname: "<S9>/SS1_OUT"};
	this.rtwnameHashMap["<S9>/SS2_OUT"] = {sid: "IP_Core_SS_Switch_and_PWM:24"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:24"] = {rtwname: "<S9>/SS2_OUT"};
	this.rtwnameHashMap["<S9>/SS3_OUT"] = {sid: "IP_Core_SS_Switch_and_PWM:25"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:25"] = {rtwname: "<S9>/SS3_OUT"};
	this.rtwnameHashMap["<S9>/SS4_OUT"] = {sid: "IP_Core_SS_Switch_and_PWM:26"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:26"] = {rtwname: "<S9>/SS4_OUT"};
	this.rtwnameHashMap["<S9>/SS5_OUT"] = {sid: "IP_Core_SS_Switch_and_PWM:27"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:27"] = {rtwname: "<S9>/SS5_OUT"};
	this.rtwnameHashMap["<S10>/hdl_cnt"] = {sid: "IP_Core_SS_Switch_and_PWM:389"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:389"] = {rtwname: "<S10>/hdl_cnt"};
	this.rtwnameHashMap["<S10>/f_carrier_kHz"] = {sid: "IP_Core_SS_Switch_and_PWM:390"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:390"] = {rtwname: "<S10>/f_carrier_kHz"};
	this.rtwnameHashMap["<S10>/T_carrier_us"] = {sid: "IP_Core_SS_Switch_and_PWM:391"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:391"] = {rtwname: "<S10>/T_carrier_us"};
	this.rtwnameHashMap["<S10>/Constant1"] = {sid: "IP_Core_SS_Switch_and_PWM:542"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:542"] = {rtwname: "<S10>/Constant1"};
	this.rtwnameHashMap["<S10>/Constant11"] = {sid: "IP_Core_SS_Switch_and_PWM:392"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:392"] = {rtwname: "<S10>/Constant11"};
	this.rtwnameHashMap["<S10>/Constant12"] = {sid: "IP_Core_SS_Switch_and_PWM:393"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:393"] = {rtwname: "<S10>/Constant12"};
	this.rtwnameHashMap["<S10>/Constant14"] = {sid: "IP_Core_SS_Switch_and_PWM:394"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:394"] = {rtwname: "<S10>/Constant14"};
	this.rtwnameHashMap["<S10>/Delay"] = {sid: "IP_Core_SS_Switch_and_PWM:395"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:395"] = {rtwname: "<S10>/Delay"};
	this.rtwnameHashMap["<S10>/Delay6"] = {sid: "IP_Core_SS_Switch_and_PWM:396"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:396"] = {rtwname: "<S10>/Delay6"};
	this.rtwnameHashMap["<S10>/Logical Operator"] = {sid: "IP_Core_SS_Switch_and_PWM:399"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:399"] = {rtwname: "<S10>/Logical Operator"};
	this.rtwnameHashMap["<S10>/Product"] = {sid: "IP_Core_SS_Switch_and_PWM:400"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:400"] = {rtwname: "<S10>/Product"};
	this.rtwnameHashMap["<S10>/Relational Operator1"] = {sid: "IP_Core_SS_Switch_and_PWM:510"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:510"] = {rtwname: "<S10>/Relational Operator1"};
	this.rtwnameHashMap["<S10>/Relational Operator2"] = {sid: "IP_Core_SS_Switch_and_PWM:541"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:541"] = {rtwname: "<S10>/Relational Operator2"};
	this.rtwnameHashMap["<S10>/Relational Operator4"] = {sid: "IP_Core_SS_Switch_and_PWM:401"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:401"] = {rtwname: "<S10>/Relational Operator4"};
	this.rtwnameHashMap["<S10>/Relational Operator5"] = {sid: "IP_Core_SS_Switch_and_PWM:402"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:402"] = {rtwname: "<S10>/Relational Operator5"};
	this.rtwnameHashMap["<S10>/Scope9"] = {sid: "IP_Core_SS_Switch_and_PWM:407"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:407"] = {rtwname: "<S10>/Scope9"};
	this.rtwnameHashMap["<S10>/Switch6"] = {sid: "IP_Core_SS_Switch_and_PWM:408"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:408"] = {rtwname: "<S10>/Switch6"};
	this.rtwnameHashMap["<S10>/Switch7"] = {sid: "IP_Core_SS_Switch_and_PWM:409"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:409"] = {rtwname: "<S10>/Switch7"};
	this.rtwnameHashMap["<S10>/dir_ctrl"] = {sid: "IP_Core_SS_Switch_and_PWM:410"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:410"] = {rtwname: "<S10>/dir_ctrl"};
	this.rtwnameHashMap["<S10>/triangle_sig"] = {sid: "IP_Core_SS_Switch_and_PWM:411"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:411"] = {rtwname: "<S10>/triangle_sig"};
	this.rtwnameHashMap["<S10>/Period_CenterMax"] = {sid: "IP_Core_SS_Switch_and_PWM:509"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:509"] = {rtwname: "<S10>/Period_CenterMax"};
	this.rtwnameHashMap["<S10>/Period_CenterMin"] = {sid: "IP_Core_SS_Switch_and_PWM:540"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:540"] = {rtwname: "<S10>/Period_CenterMin"};
	this.rtwnameHashMap["<S11>/enb"] = {sid: "IP_Core_SS_Switch_and_PWM:425:233"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:233"] = {rtwname: "<S11>/enb"};
	this.rtwnameHashMap["<S11>/dir"] = {sid: "IP_Core_SS_Switch_and_PWM:425:234"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:234"] = {rtwname: "<S11>/dir"};
	this.rtwnameHashMap["<S11>/Add_wrap"] = {sid: "IP_Core_SS_Switch_and_PWM:425:1"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:1"] = {rtwname: "<S11>/Add_wrap"};
	this.rtwnameHashMap["<S11>/Constant"] = {sid: "IP_Core_SS_Switch_and_PWM:425:212"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:212"] = {rtwname: "<S11>/Constant"};
	this.rtwnameHashMap["<S11>/Count_reg"] = {sid: "IP_Core_SS_Switch_and_PWM:425:9"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:9"] = {rtwname: "<S11>/Count_reg"};
	this.rtwnameHashMap["<S11>/DT_convert"] = {sid: "IP_Core_SS_Switch_and_PWM:425:10"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:10"] = {rtwname: "<S11>/DT_convert"};
	this.rtwnameHashMap["<S11>/DT_convert1"] = {sid: "IP_Core_SS_Switch_and_PWM:425:11"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:11"] = {rtwname: "<S11>/DT_convert1"};
	this.rtwnameHashMap["<S11>/Dir_logic"] = {sid: "IP_Core_SS_Switch_and_PWM:425:12"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:12"] = {rtwname: "<S11>/Dir_logic"};
	this.rtwnameHashMap["<S11>/Free_running_or_modulo"] = {sid: "IP_Core_SS_Switch_and_PWM:425:17"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:17"] = {rtwname: "<S11>/Free_running_or_modulo"};
	this.rtwnameHashMap["<S11>/From_value"] = {sid: "IP_Core_SS_Switch_and_PWM:425:19"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:19"] = {rtwname: "<S11>/From_value"};
	this.rtwnameHashMap["<S11>/Init_value"] = {sid: "IP_Core_SS_Switch_and_PWM:425:18"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:18"] = {rtwname: "<S11>/Init_value"};
	this.rtwnameHashMap["<S11>/Relational Operator"] = {sid: "IP_Core_SS_Switch_and_PWM:425:211"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:211"] = {rtwname: "<S11>/Relational Operator"};
	this.rtwnameHashMap["<S11>/Signal Specification"] = {sid: "IP_Core_SS_Switch_and_PWM:425:210"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:210"] = {rtwname: "<S11>/Signal Specification"};
	this.rtwnameHashMap["<S11>/Step_value"] = {sid: "IP_Core_SS_Switch_and_PWM:425:21"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:21"] = {rtwname: "<S11>/Step_value"};
	this.rtwnameHashMap["<S11>/Sub_wrap"] = {sid: "IP_Core_SS_Switch_and_PWM:425:22"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:22"] = {rtwname: "<S11>/Sub_wrap"};
	this.rtwnameHashMap["<S11>/Switch_dir"] = {sid: "IP_Core_SS_Switch_and_PWM:425:30"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:30"] = {rtwname: "<S11>/Switch_dir"};
	this.rtwnameHashMap["<S11>/Switch_enb"] = {sid: "IP_Core_SS_Switch_and_PWM:425:31"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:31"] = {rtwname: "<S11>/Switch_enb"};
	this.rtwnameHashMap["<S11>/Switch_load"] = {sid: "IP_Core_SS_Switch_and_PWM:425:32"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:32"] = {rtwname: "<S11>/Switch_load"};
	this.rtwnameHashMap["<S11>/Switch_max"] = {sid: "IP_Core_SS_Switch_and_PWM:425:33"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:33"] = {rtwname: "<S11>/Switch_max"};
	this.rtwnameHashMap["<S11>/Switch_reset"] = {sid: "IP_Core_SS_Switch_and_PWM:425:34"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:34"] = {rtwname: "<S11>/Switch_reset"};
	this.rtwnameHashMap["<S11>/Switch_type"] = {sid: "IP_Core_SS_Switch_and_PWM:425:35"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:35"] = {rtwname: "<S11>/Switch_type"};
	this.rtwnameHashMap["<S11>/const_load"] = {sid: "IP_Core_SS_Switch_and_PWM:425:38"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:38"] = {rtwname: "<S11>/const_load"};
	this.rtwnameHashMap["<S11>/const_load_val"] = {sid: "IP_Core_SS_Switch_and_PWM:425:39"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:39"] = {rtwname: "<S11>/const_load_val"};
	this.rtwnameHashMap["<S11>/const_rst"] = {sid: "IP_Core_SS_Switch_and_PWM:425:40"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:40"] = {rtwname: "<S11>/const_rst"};
	this.rtwnameHashMap["<S11>/count_hit_subsystem"] = {sid: "IP_Core_SS_Switch_and_PWM:425:221"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:221"] = {rtwname: "<S11>/count_hit_subsystem"};
	this.rtwnameHashMap["<S11>/count_hit_terminator"] = {sid: "IP_Core_SS_Switch_and_PWM:425:220"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:220"] = {rtwname: "<S11>/count_hit_terminator"};
	this.rtwnameHashMap["<S11>/count"] = {sid: "IP_Core_SS_Switch_and_PWM:425:41"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:41"] = {rtwname: "<S11>/count"};
	this.rtwnameHashMap["<S12>/ref_sig"] = {sid: "IP_Core_SS_Switch_and_PWM:427"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:427"] = {rtwname: "<S12>/ref_sig"};
	this.rtwnameHashMap["<S12>/min_pulse_width"] = {sid: "IP_Core_SS_Switch_and_PWM:428"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:428"] = {rtwname: "<S12>/min_pulse_width"};
	this.rtwnameHashMap["<S12>/Add"] = {sid: "IP_Core_SS_Switch_and_PWM:429"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:429"] = {rtwname: "<S12>/Add"};
	this.rtwnameHashMap["<S12>/Constant"] = {sid: "IP_Core_SS_Switch_and_PWM:430"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:430"] = {rtwname: "<S12>/Constant"};
	this.rtwnameHashMap["<S12>/Constant1"] = {sid: "IP_Core_SS_Switch_and_PWM:431"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:431"] = {rtwname: "<S12>/Constant1"};
	this.rtwnameHashMap["<S12>/Constant2"] = {sid: "IP_Core_SS_Switch_and_PWM:432"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:432"] = {rtwname: "<S12>/Constant2"};
	this.rtwnameHashMap["<S12>/Logical Operator"] = {sid: "IP_Core_SS_Switch_and_PWM:433"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:433"] = {rtwname: "<S12>/Logical Operator"};
	this.rtwnameHashMap["<S12>/Relational Operator"] = {sid: "IP_Core_SS_Switch_and_PWM:434"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:434"] = {rtwname: "<S12>/Relational Operator"};
	this.rtwnameHashMap["<S12>/Relational Operator1"] = {sid: "IP_Core_SS_Switch_and_PWM:435"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:435"] = {rtwname: "<S12>/Relational Operator1"};
	this.rtwnameHashMap["<S12>/Saturation"] = {sid: "IP_Core_SS_Switch_and_PWM:436"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:436"] = {rtwname: "<S12>/Saturation"};
	this.rtwnameHashMap["<S12>/Switch1"] = {sid: "IP_Core_SS_Switch_and_PWM:437"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:437"] = {rtwname: "<S12>/Switch1"};
	this.rtwnameHashMap["<S12>/Switch6"] = {sid: "IP_Core_SS_Switch_and_PWM:438"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:438"] = {rtwname: "<S12>/Switch6"};
	this.rtwnameHashMap["<S12>/ref_sig_limit"] = {sid: "IP_Core_SS_Switch_and_PWM:439"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:439"] = {rtwname: "<S12>/ref_sig_limit"};
	this.rtwnameHashMap["<S13>/ref_sig"] = {sid: "IP_Core_SS_Switch_and_PWM:442"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:442"] = {rtwname: "<S13>/ref_sig"};
	this.rtwnameHashMap["<S13>/min_pulse_width"] = {sid: "IP_Core_SS_Switch_and_PWM:443"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:443"] = {rtwname: "<S13>/min_pulse_width"};
	this.rtwnameHashMap["<S13>/Add"] = {sid: "IP_Core_SS_Switch_and_PWM:444"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:444"] = {rtwname: "<S13>/Add"};
	this.rtwnameHashMap["<S13>/Constant"] = {sid: "IP_Core_SS_Switch_and_PWM:445"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:445"] = {rtwname: "<S13>/Constant"};
	this.rtwnameHashMap["<S13>/Constant1"] = {sid: "IP_Core_SS_Switch_and_PWM:446"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:446"] = {rtwname: "<S13>/Constant1"};
	this.rtwnameHashMap["<S13>/Constant2"] = {sid: "IP_Core_SS_Switch_and_PWM:447"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:447"] = {rtwname: "<S13>/Constant2"};
	this.rtwnameHashMap["<S13>/Logical Operator"] = {sid: "IP_Core_SS_Switch_and_PWM:448"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:448"] = {rtwname: "<S13>/Logical Operator"};
	this.rtwnameHashMap["<S13>/Relational Operator"] = {sid: "IP_Core_SS_Switch_and_PWM:449"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:449"] = {rtwname: "<S13>/Relational Operator"};
	this.rtwnameHashMap["<S13>/Relational Operator1"] = {sid: "IP_Core_SS_Switch_and_PWM:450"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:450"] = {rtwname: "<S13>/Relational Operator1"};
	this.rtwnameHashMap["<S13>/Saturation"] = {sid: "IP_Core_SS_Switch_and_PWM:451"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:451"] = {rtwname: "<S13>/Saturation"};
	this.rtwnameHashMap["<S13>/Switch1"] = {sid: "IP_Core_SS_Switch_and_PWM:452"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:452"] = {rtwname: "<S13>/Switch1"};
	this.rtwnameHashMap["<S13>/Switch6"] = {sid: "IP_Core_SS_Switch_and_PWM:453"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:453"] = {rtwname: "<S13>/Switch6"};
	this.rtwnameHashMap["<S13>/ref_sig_limit"] = {sid: "IP_Core_SS_Switch_and_PWM:454"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:454"] = {rtwname: "<S13>/ref_sig_limit"};
	this.rtwnameHashMap["<S14>/ref_sig"] = {sid: "IP_Core_SS_Switch_and_PWM:457"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:457"] = {rtwname: "<S14>/ref_sig"};
	this.rtwnameHashMap["<S14>/min_pulse_width"] = {sid: "IP_Core_SS_Switch_and_PWM:458"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:458"] = {rtwname: "<S14>/min_pulse_width"};
	this.rtwnameHashMap["<S14>/Add"] = {sid: "IP_Core_SS_Switch_and_PWM:459"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:459"] = {rtwname: "<S14>/Add"};
	this.rtwnameHashMap["<S14>/Constant"] = {sid: "IP_Core_SS_Switch_and_PWM:460"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:460"] = {rtwname: "<S14>/Constant"};
	this.rtwnameHashMap["<S14>/Constant1"] = {sid: "IP_Core_SS_Switch_and_PWM:461"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:461"] = {rtwname: "<S14>/Constant1"};
	this.rtwnameHashMap["<S14>/Constant2"] = {sid: "IP_Core_SS_Switch_and_PWM:462"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:462"] = {rtwname: "<S14>/Constant2"};
	this.rtwnameHashMap["<S14>/Logical Operator"] = {sid: "IP_Core_SS_Switch_and_PWM:463"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:463"] = {rtwname: "<S14>/Logical Operator"};
	this.rtwnameHashMap["<S14>/Relational Operator"] = {sid: "IP_Core_SS_Switch_and_PWM:464"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:464"] = {rtwname: "<S14>/Relational Operator"};
	this.rtwnameHashMap["<S14>/Relational Operator1"] = {sid: "IP_Core_SS_Switch_and_PWM:465"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:465"] = {rtwname: "<S14>/Relational Operator1"};
	this.rtwnameHashMap["<S14>/Saturation"] = {sid: "IP_Core_SS_Switch_and_PWM:466"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:466"] = {rtwname: "<S14>/Saturation"};
	this.rtwnameHashMap["<S14>/Switch1"] = {sid: "IP_Core_SS_Switch_and_PWM:467"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:467"] = {rtwname: "<S14>/Switch1"};
	this.rtwnameHashMap["<S14>/Switch6"] = {sid: "IP_Core_SS_Switch_and_PWM:468"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:468"] = {rtwname: "<S14>/Switch6"};
	this.rtwnameHashMap["<S14>/ref_sig_limit"] = {sid: "IP_Core_SS_Switch_and_PWM:469"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:469"] = {rtwname: "<S14>/ref_sig_limit"};
	this.rtwnameHashMap["<S15>/step"] = {sid: "IP_Core_SS_Switch_and_PWM:425:2"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:2"] = {rtwname: "<S15>/step"};
	this.rtwnameHashMap["<S15>/fb"] = {sid: "IP_Core_SS_Switch_and_PWM:425:3"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:3"] = {rtwname: "<S15>/fb"};
	this.rtwnameHashMap["<S15>/Add"] = {sid: "IP_Core_SS_Switch_and_PWM:425:4"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:4"] = {rtwname: "<S15>/Add"};
	this.rtwnameHashMap["<S15>/Compare To Constant"] = {sid: "IP_Core_SS_Switch_and_PWM:425:215"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:215"] = {rtwname: "<S15>/Compare To Constant"};
	this.rtwnameHashMap["<S15>/Mod_value"] = {sid: "IP_Core_SS_Switch_and_PWM:425:5"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:5"] = {rtwname: "<S15>/Mod_value"};
	this.rtwnameHashMap["<S15>/Switch_wrap"] = {sid: "IP_Core_SS_Switch_and_PWM:425:6"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:6"] = {rtwname: "<S15>/Switch_wrap"};
	this.rtwnameHashMap["<S15>/Wrap"] = {sid: "IP_Core_SS_Switch_and_PWM:425:7"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:7"] = {rtwname: "<S15>/Wrap"};
	this.rtwnameHashMap["<S15>/add"] = {sid: "IP_Core_SS_Switch_and_PWM:425:8"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:8"] = {rtwname: "<S15>/add"};
	this.rtwnameHashMap["<S15>/count_hit"] = {sid: "IP_Core_SS_Switch_and_PWM:425:216"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:216"] = {rtwname: "<S15>/count_hit"};
	this.rtwnameHashMap["<S16>/dir_port"] = {sid: "IP_Core_SS_Switch_and_PWM:425:13"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:13"] = {rtwname: "<S16>/dir_port"};
	this.rtwnameHashMap["<S16>/Logical Operator"] = {sid: "IP_Core_SS_Switch_and_PWM:425:14"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:14"] = {rtwname: "<S16>/Logical Operator"};
	this.rtwnameHashMap["<S16>/Pos_step"] = {sid: "IP_Core_SS_Switch_and_PWM:425:15"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:15"] = {rtwname: "<S16>/Pos_step"};
	this.rtwnameHashMap["<S16>/dn"] = {sid: "IP_Core_SS_Switch_and_PWM:425:16"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:16"] = {rtwname: "<S16>/dn"};
	this.rtwnameHashMap["<S17>/step"] = {sid: "IP_Core_SS_Switch_and_PWM:425:23"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:23"] = {rtwname: "<S17>/step"};
	this.rtwnameHashMap["<S17>/fb"] = {sid: "IP_Core_SS_Switch_and_PWM:425:24"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:24"] = {rtwname: "<S17>/fb"};
	this.rtwnameHashMap["<S17>/Add"] = {sid: "IP_Core_SS_Switch_and_PWM:425:25"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:25"] = {rtwname: "<S17>/Add"};
	this.rtwnameHashMap["<S17>/Compare To Constant"] = {sid: "IP_Core_SS_Switch_and_PWM:425:213"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:213"] = {rtwname: "<S17>/Compare To Constant"};
	this.rtwnameHashMap["<S17>/Mod_value"] = {sid: "IP_Core_SS_Switch_and_PWM:425:26"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:26"] = {rtwname: "<S17>/Mod_value"};
	this.rtwnameHashMap["<S17>/Switch_wrap"] = {sid: "IP_Core_SS_Switch_and_PWM:425:27"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:27"] = {rtwname: "<S17>/Switch_wrap"};
	this.rtwnameHashMap["<S17>/Wrap"] = {sid: "IP_Core_SS_Switch_and_PWM:425:28"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:28"] = {rtwname: "<S17>/Wrap"};
	this.rtwnameHashMap["<S17>/sub"] = {sid: "IP_Core_SS_Switch_and_PWM:425:29"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:29"] = {rtwname: "<S17>/sub"};
	this.rtwnameHashMap["<S17>/count_hit"] = {sid: "IP_Core_SS_Switch_and_PWM:425:214"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:214"] = {rtwname: "<S17>/count_hit"};
	this.rtwnameHashMap["<S18>/u"] = {sid: "IP_Core_SS_Switch_and_PWM:425:215:1"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:215:1"] = {rtwname: "<S18>/u"};
	this.rtwnameHashMap["<S18>/Compare"] = {sid: "IP_Core_SS_Switch_and_PWM:425:215:2"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:215:2"] = {rtwname: "<S18>/Compare"};
	this.rtwnameHashMap["<S18>/Constant"] = {sid: "IP_Core_SS_Switch_and_PWM:425:215:3"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:215:3"] = {rtwname: "<S18>/Constant"};
	this.rtwnameHashMap["<S18>/y"] = {sid: "IP_Core_SS_Switch_and_PWM:425:215:4"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:215:4"] = {rtwname: "<S18>/y"};
	this.rtwnameHashMap["<S19>/u"] = {sid: "IP_Core_SS_Switch_and_PWM:425:213:1"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:213:1"] = {rtwname: "<S19>/u"};
	this.rtwnameHashMap["<S19>/Compare"] = {sid: "IP_Core_SS_Switch_and_PWM:425:213:2"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:213:2"] = {rtwname: "<S19>/Compare"};
	this.rtwnameHashMap["<S19>/Constant"] = {sid: "IP_Core_SS_Switch_and_PWM:425:213:3"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:213:3"] = {rtwname: "<S19>/Constant"};
	this.rtwnameHashMap["<S19>/y"] = {sid: "IP_Core_SS_Switch_and_PWM:425:213:4"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:213:4"] = {rtwname: "<S19>/y"};
	this.getSID = function(rtwname) { return this.rtwnameHashMap[rtwname];}
	this.getRtwname = function(sid) { return this.sidHashMap[sid];}
}
RTW_rtwnameSIDMap.instance = new RTW_rtwnameSIDMap();
